<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh Looping</h1>
    <?php 
        echo"<h3>Contoh 1</h3>";

        echo"<h4>Looping 1</h4>";
        for ($a=1; $a<=19 ; $a+=2) {
            echo $a. "-Angka Ganjil 1-19<br>";
        }

        echo"<h4>Looping 2</h4>";
        $x =19;

        do {
            echo"$x -Angka Ganjil 19 sampai 1<br>";
            $x-=2;
        } while ($x >= 1);

        echo "<h3>Contoh 2</h3>";
        $nomor = [60,70,84,56,74,32];
        echo "Nomor :";
        print_r($nomor);
        foreach($nomor as $value){
            $item[] = $value%4;
        }
        echo"<br>";
        echo "Sisa bagi 4 Nomor :";
        print_r($item);

        echo"<h3>Contoh 3</h3>";

        $biodata =[
            ["Rezky",25,"Makasar"],
            ["Abduh",28,"Bandung"],
            ["Hanif",26,"Jakarta"]
        ];
        foreach($biodata as $key =>$value){
            $tampung =[
                'nama' => $value[0],
                'umur' => $value[1],
                'Alamat' => $value[2],
            ];
            print_r($tampung);
            echo"<br>";
        }

        echo "<h3>Contoh 4</h3>";

        for ($j=1; $j <= 5 ; $j++) { 
            for ($k=$j; $k <=5 ; $k++) { 
                echo "*";
            }
            for ($i=$j; $i <= 5; $i++){
                echo "#";
            }
        }
    ?>
</body> 
</html>